view: mock_product_region_install {
  sql_table_name: `poc.mock_product_region_install`
    ;;

  dimension: installCount {
    hidden: yes
    type: number
    sql: ${TABLE}.installCount ;;
  }

  dimension: product {
    type: string
    sql: ${TABLE}.product ;;
    drill_fields: [region, install_count]
  }

  dimension: region {
    type: string
    sql: ${TABLE}.region ;;
    drill_fields: [product, install_count]
  }

  measure: count {
    type: count
    drill_fields: []
  }

  measure: install_count {
    type: sum
    drill_fields: [product, region, install_count]
    sql: ${installCount} ;;
  }
}
