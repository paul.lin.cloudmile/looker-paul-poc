connection: "paul-poc-connection"

# include all the views
include: "/views/**/*.view"

datagroup: paul_poc_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: paul_poc_default_datagroup

explore: mock_product_region_install {}

# explore: cm_project_owner {}

# explore: cm_project_planning_snapshot {}

# explore: mocked_project_planning {}

# explore: project_owner {}
